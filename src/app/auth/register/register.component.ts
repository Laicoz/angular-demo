import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user';
import { AuthService } from '../auth-services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  model = new User();
  nameTaken: boolean = false;

  constructor(    
    private authService: AuthService,
    private router: Router
    ) { }

  ngOnInit(): void {
  }

  onSubmit(){
    console.log("On submit:");
    console.log(this.model)
    this.authService.register(this.model.name, this.model.password).subscribe((user) => {
      if (user) {
        this.router.navigate(['/']);
      } else{
        this.nameTaken = true;
      }
    });
  }


}
