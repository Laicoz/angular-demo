import { Injectable } from '@angular/core';

import {BehaviorSubject, Observable, of} from 'rxjs';

import {User} from '../user';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { AlertService } from '../../alert/alert-services/alert.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private authUrl = 'https://dungeonshareapi.herokuapp.com'

  public currentUser$ = new BehaviorSubject<User>(undefined);

  private readonly CURRENT_USER = 'currentuser';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  }

  private handleError<T>(operation = 'operation', result?:T){
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }

  constructor(
    private http: HttpClient,
    private alertService: AlertService
  ) { }


  login(name: string, password: string): Observable<User>{
    console.log('service login called 1.');

    return this.http
      .post(
        `${this.authUrl}/authentication/login`, 
        {name, password}, 
        this.httpOptions)
    .pipe(
      map((response: any) => {
        const user = {...response} as User;
        console.log("service login 2:", user)
        this.saveUserToLocalStorage(user);
        this.currentUser$.next(user);

        return user;
      }),
      catchError((error: any) => {
        console.log('error:', error);
        console.log('error.message:', error.message);
        console.log('error.error.message:', error.error.message);
        this.alertService.error(error.error.message || error.message);
        return of(undefined);
      })
    )
  }

  register(name: string, password: string): Observable<User>{
    console.log('Register called.');

    return this.http
      .post(
        `${this.authUrl}/authentication/register`, 
        {name, password}, 
        this.httpOptions)
    .pipe(
      map((response: any) => {
        const user = {...response} as User;
        this.saveUserToLocalStorage(user);
        this.currentUser$.next(user);

        return user;
      }),
      catchError((error: any) => {
        console.log('error:', error);
        console.log('error.message:', error.message);
        console.log('error.error.message:', error.error.message);
        this.alertService.error(error.error.message || error.message);
        return of(undefined);
      })
    )

  }


  logout(): void {
    console.log('logout called.');
    localStorage.setItem(this.CURRENT_USER, null);
    this.currentUser$ = new BehaviorSubject<User>(undefined);
  }

  userMayEdit(itemUserId: string): Observable<boolean>{
    return this.currentUser$.pipe(
      map((user: User) => (user ? user._id === itemUserId: false))
    );
  }

  getUserFromLocalStorage(): Observable<User> {
    const localUser = JSON.parse(localStorage.getItem(this.CURRENT_USER));
    return of(localUser);
  }

  private saveUserToLocalStorage(user: User): void{
    localStorage.setItem(this.CURRENT_USER, JSON.stringify(user));
  }

}
