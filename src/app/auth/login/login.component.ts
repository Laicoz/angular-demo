import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user';
import { AuthService } from '../auth-services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  model = new User();
  credentialsTrue: boolean = true; 

  constructor(
    private authService: AuthService,
    private router: Router
    ) {}

  ngOnInit(): void {
    
  }

  onSubmit(){
    console.log("On submit:");
    console.log(this.model)
    this.authService.login(this.model.name, this.model.password).subscribe((user) => {
      if (user) {
        this.router.navigate(['/']);
      } else {
        this.credentialsTrue = false;
      }
    });
  }

}
