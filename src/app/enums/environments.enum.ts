export enum Environments {
    Forest = 'forest',
    Mountains = 'mountains',
    Cave = 'cave',
    Castle = 'castle',
    Undecided = 'undecided'

}
