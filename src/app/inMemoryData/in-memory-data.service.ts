import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Dungeon } from '../pages/dungeon/dungeon';
import { Environments } from '../enums/environments.enum'

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const dungeons = [
      {id: "1", name: "My first dungeon", description: "A dungeon for beginners.", environment: Environments.Forest, difficulty: 1, prefferedNumberOfPlayers: 4},
    {id: "2", name: "My second dungeon", description: "A dungeon for beginners.", environment: Environments.Castle, difficulty: 1, prefferedNumberOfPlayers: 4},
    {id: "3", name: "My third dungeon", description: "A dungeon for beginners.", environment: Environments.Undecided, difficulty: 1, prefferedNumberOfPlayers: 4},
    {id: "4", name: "My fourth dungeon", description: "A dungeon for beginners.", environment: Environments.Mountains, difficulty: 1, prefferedNumberOfPlayers: 4},
    {id: "5", name: "My fifth dungeon", description: "A dungeon for beginners.", environment: Environments.Cave, difficulty: 1, prefferedNumberOfPlayers: 4}
    ];
    return {dungeons};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  // genId(dungeons: Dungeon[]): number {
  //   return dungeons.length > 0 ? Math.max(...dungeons.map(dungeon => dungeon.id)) + 1 : 11;
  // }
}