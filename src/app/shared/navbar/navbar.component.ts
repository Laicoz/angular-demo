import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../auth/user'
import { AuthService } from '../../auth/auth-services/auth.service'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  @Input() title: string;
  isNavbarCollapsed = true;
  loggedInUser$: Observable<User>
  name: string;
  

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.loggedInUser$ = this.authService.currentUser$; 
    this.loggedInUser$.subscribe(value => this.name = value.name)

    // Meerdere manieren om value te lezen.
    // this.loggedInUser$ = this.authService.currentUser$; 
    // console.log("vanaf navbar authservice.currentuser", this.authService.currentUser$)
    // this.loggedInUser$.subscribe(value => this.name = value.name) <== volgens mij de beste.
    // console.log("navbar: ", this.name) <==
    // if(!(this.authService.currentUser$.value == undefined)){
    //   console.log("vanaf navbar name", this.authService.currentUser$.value.name);
    //   this.name = this.authService.currentUser$.value.name;
    // }
  }

  logout(): void {
    this.authService.logout();
    this.loggedInUser$ = null;
  }
}
