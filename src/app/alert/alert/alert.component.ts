import { Component, OnInit } from '@angular/core';
import { AlertService } from '../alert-services/alert.service';


@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  subs;
  alert;
  staticAlertClosed;

  constructor(private alertService: AlertService) { }

  ngOnInit(): void {
    this.subs = this.alertService.alert$.subscribe((alert) => {
      this.alert = alert;
      this.staticAlertClosed = false;
      setTimeout(() => (this.staticAlertClosed = true), 6000);
    });
  }

  ngOnDestroy(): void{
    this.subs.unsubscribe();
  }

}
