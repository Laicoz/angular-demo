import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from './index.monster';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MinDirective } from './monster-directives/min.directive';
import { MaxDirective } from './monster-directives/max.directive';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: fromComponents.MonsterListComponent},
  { path: 'add', pathMatch: 'full', component: fromComponents.MonsterAddComponent},
  { path: ':monsterId', pathMatch: 'full', component: fromComponents.MonsterDetailComponent},
];

@NgModule({
  declarations: [...fromComponents.components, MinDirective, MaxDirective],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule, 
    RouterModule.forChild(routes),
  ]
})
export class MonsterModule { }