import { Injectable } from '@angular/core';

import {Observable, of} from 'rxjs';

import {Monster} from '../monster';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MonsterService {

  private monstersUrl = 'https://dungeonshareapi.herokuapp.com/monster'

  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      Authorization: 'placeholder' 
    })
  }

  private handleError<T>(operation = 'operation', result?:T){
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }

  constructor(
    private http: HttpClient
  ) { }

  getMonsters(): Observable<Monster[]> {
    return this.http.get<Monster[]>(this.monstersUrl)
      .pipe(
        catchError(this.handleError<Monster[]>('getMonsters', []))
      )
  }

  getMonster(id: string): Observable<Monster>{
    const url = `${this.monstersUrl}/${id}`;
    return this.http.get<Monster>(url).pipe(
      tap(console.log),
      catchError(this.handleError<Monster>(`getMonster id=${id}`))
    );
  }

  addMonster(monster: Monster, token: string): Observable<Monster>{
    console.log(token);
    this.setHttpOptions(token);

    return this.http.post<Monster>(this.monstersUrl, monster, this.httpOptions).pipe(
      catchError(this.handleError<Monster>('addMonster'))
    );
  }

  deleteMonster(monster: Monster | string, token: string): Observable<Monster>{
    this.setHttpOptions(token);
    const id = typeof monster === 'string' ? monster : monster._id;
    const url = `${this.monstersUrl}/${id}`;

    return this.http.delete<Monster>(url, this.httpOptions).pipe(
      catchError(this.handleError<Monster>('deleteMonster'))
    );
  }

  setHttpOptions(token: String){
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Bearer ${token}`);
  }

}
