import { MonsterAddComponent } from './monster-add/monster-add.component';
import { MonsterListComponent } from './monster-list/monster-list.component';
import { MonsterDetailComponent } from './monster-detail/monster-detail.component';

export const components: any[] = [
    MonsterAddComponent,
    MonsterListComponent,
    MonsterDetailComponent
];

export * from './monster-add/monster-add.component';
export * from './monster-list/monster-list.component';
export * from './monster-detail/monster-detail.component';