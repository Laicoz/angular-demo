import { Component, OnInit } from '@angular/core';
import { MonsterService } from '../monster-services/monster.service';
import { AuthService} from '../../../auth/auth-services/auth.service';

import { Type } from '../../../enums/type.enum';
import { Size } from '../../../enums/size.enum';
import { Monster } from '../monster';
import { Location } from '@angular/common';

import { Observable } from 'rxjs';
import { User } from '../../../auth/user';

@Component({
  selector: 'app-monster-add',
  templateUrl: './monster-add.component.html',
  styleUrls: ['./monster-add.component.css']
})
export class MonsterAddComponent implements OnInit {

  sizes = ['tiny', 'small', 'medium', 'large', 'huge', 'gargantuan']
  types = ['abberation', 'beast', 'celestial', 'construct', 'dragon', 'elemental', 'fey', 'fiend', 'giant', 'humanoid', 'monstrosity', 'ooze', 'plant', 'undead']

  model = new Monster("", Size.Medium, Type.Humanoid, 1, 10, 10, "Monster Manual")

  loggedInUser$: Observable<User>
  token: string;

  constructor(
    private monsterService: MonsterService,
    private location: Location,
    private authService: AuthService
  ) { 

  }

  ngOnInit(): void {
    this.loggedInUser$ = this.authService.currentUser$; 
    this.loggedInUser$.subscribe(value => this.token = value.token)
  }

  onSubmit(){
    console.log("On submit:");
    console.log(this.model);
    this.monsterService.addMonster(this.model, this.token)
      .subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }

}