import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Monster } from '../monster';
import { MonsterService } from '../monster-services/monster.service';
import { Observable } from 'rxjs';
import { User } from '../../../auth/user'
import { AuthService } from 'src/app/auth/auth-services/auth.service';

@Component({
  selector: 'app-monster-detail',
  templateUrl: './monster-detail.component.html',
  styleUrls: ['./monster-detail.component.css']
})
export class MonsterDetailComponent implements OnInit {
  
  @Input() monster: Monster;
  userId: string;
  token: string;

  constructor(
    private route: ActivatedRoute,
    private monsterService: MonsterService,
    private location: Location,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.getMonster();
    this.userId = this.authService.currentUser$.value._id; 
    this.token = this.authService.currentUser$.value.token;
  }

  getMonster(): void{
    const id = this.route.snapshot.paramMap.get('monsterId');
    this.monsterService.getMonster(id)
      .subscribe(monster => this.monster = monster);
  }

  delete(monster: Monster): void{
    this.monsterService.deleteMonster(monster, this.token)
      .subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }

}
