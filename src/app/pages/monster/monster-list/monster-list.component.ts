import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../../auth/user'
import {Monster} from '../monster';
import {MonsterService} from '../monster-services/monster.service';
import { AuthService} from '../../../auth/auth-services/auth.service'

@Component({
  selector: 'app-monster-list',
  templateUrl: './monster-list.component.html',
  styleUrls: ['./monster-list.component.css']
})
export class MonsterListComponent implements OnInit {

  monsters: Monster[];
  loggedInUser$: Observable<User>

  constructor(
    private monsterService: MonsterService, 
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.getMonsters();
    this.loggedInUser$ = this.authService.currentUser$; 
    console.log("monster list is geladen.")
  }

  getMonsters(): void{
    this.monsterService.getMonsters()
      .subscribe(monsters => this.monsters = monsters);
  }

}
