import {Type} from '../../enums/type.enum';
import {Size} from '../../enums/size.enum';

export class Monster{

	public _id: String;
	public userId: String

    constructor(
    	public name: String,
    	public size: Size,
    	public type: Type,
    	public challenge: Number,
    	public hp: Number,
    	public ac: Number,
    	public source: String
        ){  }
}

