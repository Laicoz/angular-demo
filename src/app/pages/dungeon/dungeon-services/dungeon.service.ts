import { Injectable } from '@angular/core';

import {Observable, of} from 'rxjs';

import {Dungeon} from '../dungeon';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DungeonService {

  private dungeonsUrl = 'https://dungeonshareapi.herokuapp.com/dungeon'

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json',
    Authorization: 'placeholder'
    })
  }

  private handleError<T>(operation = 'operation', result?:T){
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }

  constructor(
    private http: HttpClient
  ) { }

  getDungeons(): Observable<Dungeon[]> {
    return this.http.get<Dungeon[]>(this.dungeonsUrl)
      .pipe(
        catchError(this.handleError<Dungeon[]>('getDungeons', []))
      )
  }

  getDungeon(id: string): Observable<Dungeon>{
    const url = `${this.dungeonsUrl}/${id}`;
    return this.http.get<Dungeon>(url).pipe(
      tap(console.log),
      catchError(this.handleError<Dungeon>(`getDungeon id=${id}`))
    );
  }

  updateDungeon(dungeon: Dungeon, token: String): Observable<any> {
    this.setHttpOptions(token)
    const url = `${this.dungeonsUrl}/${dungeon._id}`
    console.log(dungeon)
    console.log(url)
    return this.http.put(url, dungeon, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateDungeon'))
    );
  }

  addDungeon(dungeon: Dungeon, token: String): Observable<Dungeon>{
    this.setHttpOptions(token);

    return this.http.post<Dungeon>(this.dungeonsUrl, dungeon, this.httpOptions).pipe(
      catchError(this.handleError<Dungeon>('addDungeon'))
    );
  }

  deleteDungeon(dungeon: Dungeon | string, token: String): Observable<Dungeon>{
    this.setHttpOptions(token);
    const id = typeof dungeon === 'string' ? dungeon : dungeon._id;
    const url = `${this.dungeonsUrl}/${id}`;

    return this.http.delete<Dungeon>(url, this.httpOptions).pipe(
      catchError(this.handleError<Dungeon>('deleteDungeon'))
    );
  }

  setHttpOptions(token: String){
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Bearer ${token}`);
  }


}
