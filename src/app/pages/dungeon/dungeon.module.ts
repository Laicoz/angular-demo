import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from './index';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MinDirective } from './dungeon-directives/min.directive';
import { MaxDirective} from './dungeon-directives/max.directive';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: fromComponents.DungeonListComponent},
  { path: 'add', pathMatch: 'full', component: fromComponents.DungeonAddComponent},
  { path: ':dungeonId', pathMatch: 'full', component: fromComponents.DungeonDetailComponent},
  { path: ':id/edit', pathMatch: 'full', component: fromComponents.DungeonEditComponent},
];

@NgModule({
  declarations: [...fromComponents.components, MinDirective, MaxDirective],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule, 
    RouterModule.forChild(routes),
  ],
})
export class DungeonModule {}
