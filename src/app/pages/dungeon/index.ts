import { DungeonAddComponent } from './dungeon-add/dungeon-add.component';
import { DungeonListComponent } from './dungeon-list/dungeon-list.component';
import { DungeonEditComponent } from './dungeon-edit/dungeon-edit.component';
import { DungeonDetailComponent } from './dungeon-detail/dungeon-detail.component'
import { RoomListComponent } from '../room/room-list/room-list.component';

export const components: any[] = [
  DungeonAddComponent,
  DungeonListComponent,
  DungeonEditComponent,
  DungeonDetailComponent,
  RoomListComponent
];

export * from './dungeon-add/dungeon-add.component';
export * from './dungeon-list/dungeon-list.component';
export * from './dungeon-edit/dungeon-edit.component';
export * from './dungeon-detail/dungeon-detail.component';
