import { Component, Input, OnInit } from '@angular/core';
import { Dungeon } from '../dungeon';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DungeonService } from '../dungeon-services/dungeon.service';
import { AuthService} from '../../../auth/auth-services/auth.service';


@Component({
  selector: 'app-dungeon-edit',
  templateUrl: './dungeon-edit.component.html',
  styleUrls: ['./dungeon-edit.component.css']
})

export class DungeonEditComponent implements OnInit {
  @Input() dungeon: Dungeon;

  token: string;
  environments= ['forest', 'mountains', 'cave', 'castle', 'undecided'];

  constructor(
    private route: ActivatedRoute,
    private dungeonService: DungeonService,
    private location: Location,
    private authService: AuthService
  ) {
 
   }

  ngOnInit(): void {
    this.getDungeon();
    this.token = this.authService.currentUser$.value.token; 
  }

  getDungeon(): void{
    const id = this.route.snapshot.paramMap.get('id');
    this.dungeonService.getDungeon(id)
      .subscribe(dungeon => this.dungeon = dungeon);
  }

  save(): void{
    this.dungeonService.updateDungeon(this.dungeon, this.token)
      .subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }


}
