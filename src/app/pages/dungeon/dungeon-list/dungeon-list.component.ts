import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../../auth/user'
import {Dungeon} from '../dungeon';
import {DungeonService} from '../dungeon-services/dungeon.service';
import { AuthService} from '../../../auth/auth-services/auth.service'

@Component({
  selector: 'app-dungeon-list',
  templateUrl: './dungeon-list.component.html',
  styleUrls: ['./dungeon-list.component.css']
})
export class DungeonListComponent implements OnInit {

  dungeons: Dungeon[];
  loggedInUser$: Observable<User>

  constructor(
    private dungeonService: DungeonService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.getDungeons();
    this.loggedInUser$ = this.authService.currentUser$; 
    console.log("dungeon list is geladen.")
  }

  getDungeons(): void{
    this.dungeonService.getDungeons()
      .subscribe(dungeons => this.dungeons = dungeons);
  }

}
