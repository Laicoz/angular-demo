import {Environments} from '../../enums/environments.enum';
import {Room} from '../room/room'

export class Dungeon{

    public _id: String;
    public userId: String;
    public rooms: Room[];

    constructor(
        public name: String,
        public description: String,
        public environment: Environments,
        public difficulty: Number,
        public prefferedNumberOfPlayers: Number,
        ){  }
}
