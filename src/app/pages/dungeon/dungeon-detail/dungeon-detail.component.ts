import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Dungeon } from '../dungeon';
import { DungeonService } from '../dungeon-services/dungeon.service';
import { AuthService} from '../../../auth/auth-services/auth.service';

import { Observable } from 'rxjs';
import { User } from '../../../auth/user';

@Component({
  selector: 'app-dungeon-detail',
  templateUrl: './dungeon-detail.component.html',
  styleUrls: ['./dungeon-detail.component.css']
})
export class DungeonDetailComponent implements OnInit {
  @Input() dungeon: Dungeon;

  loggedInUser$: Observable<User>
  token: string;
  userId: string = null;

  constructor(
    private route: ActivatedRoute,
    private dungeonService: DungeonService,
    private location: Location,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    console.log("dungeonDetail on init called.")
    this.getDungeon();
    this.loggedInUser$ = this.authService.currentUser$; 
    this.loggedInUser$.subscribe(value => this.token = value.token)
    this.userId = this.authService.currentUser$.value._id; 
  }

  getDungeon(): void{
    const id = this.route.snapshot.paramMap.get('dungeonId');
    this.dungeonService.getDungeon(id)
      .subscribe(dungeon => this.dungeon = dungeon);
  }

  delete(dungeon: Dungeon): void{
    this.dungeonService.deleteDungeon(dungeon, this.token).
      subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }

}
