import { Component, OnInit } from '@angular/core';
import { DungeonService } from '../dungeon-services/dungeon.service';
import { AuthService} from '../../../auth/auth-services/auth.service';

import { Environments } from '../../../enums/environments.enum'
import { Dungeon } from '../dungeon'
import { Location } from '@angular/common';

import { Observable } from 'rxjs';
import { User } from '../../../auth/user';

@Component({
  selector: 'app-dungeon-add',
  templateUrl: './dungeon-add.component.html',
  styleUrls: ['./dungeon-add.component.css']
})
export class DungeonAddComponent implements OnInit {

  environments= ['forest', 'mountains', 'cave', 'castle', 'undecided'];

  model = new Dungeon("new dungeon", "description", Environments.Undecided, 0, 1);

  loggedInUser$: Observable<User>
  token: string;

  constructor(
    private dungeonService: DungeonService,
    private location: Location,
    private authService: AuthService
  ) { 

  }

  ngOnInit(): void {
    this.loggedInUser$ = this.authService.currentUser$; 
    this.loggedInUser$.subscribe(value => this.token = value.token)
  }

  onSubmit(){
    console.log("On submit:");
    console.log(this.model)
    this.dungeonService.addDungeon(this.model, this.token)
      .subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }

}
