

export class Room{

    public _id: String;
    public userId: String;

    constructor(
        public name: String,
        public description: String,
        public sizeInCubicalMeter: Number,
        public hidden: Boolean,
        ){  }
}









// export interface Room{
//     _id: String,
//     name: String,
//     description: String,
//     sizeInCubicalMeter: Number,
//     hidden: Boolean,
// }