import { Component, Input, OnInit } from '@angular/core';
import { Room } from '../room';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { RoomService } from '../room-services/room.service';
import { AuthService } from 'src/app/auth/auth-services/auth.service';

@Component({
  selector: 'app-room-edit',
  templateUrl: './room-edit.component.html',
  styleUrls: ['./room-edit.component.css']
})
export class RoomEditComponent implements OnInit {
  @Input() room: Room;

  dungeonId: string;
  roomId: string;
  token: string;

  constructor(
    private route: ActivatedRoute,
    private roomService: RoomService,
    private location: Location,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.getRoom();
    this.token = this.authService.currentUser$.value.token;
  }

  getRoom(): void{
    this.dungeonId = this.route.snapshot.paramMap.get('dungeonId');
    this.roomId = this.route.snapshot.paramMap.get('roomId');
    this.roomService.getRoom(this.dungeonId, this.roomId)
      .subscribe(room => this.room = room);
  }

  save(): void{
    this.roomService.updateRoom(this.room, this.dungeonId, this.token)
      .subscribe(() => this.goBack());
  }


  goBack(): void {
    this.location.back();
  }


}
