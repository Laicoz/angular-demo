import { RoomDetailComponent } from './room-detail/room-detail.component';
import { RoomAddComponent } from './room-add/room-add.component';
import { RoomEditComponent } from './room-edit/room-edit.component';

export const components: any[] = [
    RoomDetailComponent,
    RoomAddComponent,
    RoomEditComponent
];

export * from './room-detail/room-detail.component';
export * from './room-add/room-add.component';
export * from './room-edit/room-edit.component';
