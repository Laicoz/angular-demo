import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Room } from '../room';
import { DungeonService } from '../../dungeon/dungeon-services/dungeon.service';
import { Dungeon } from '../../dungeon/dungeon';
import { AuthService } from 'src/app/auth/auth-services/auth.service';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {

  dungeonId: string;
  rooms: Room[];
  dungeon: Dungeon; 
  userId: string;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private dungeonService: DungeonService) {

   }

  ngOnInit(): void {
    this.getRooms();
    this.userId = this.authService.currentUser$.value._id;
  }

  getRooms(): void{
    this.dungeonId = this.route.snapshot.paramMap.get('dungeonId');
      this.dungeonService.getDungeon(this.dungeonId)
      .subscribe(dungeon => this.dungeon = dungeon);
  }


}
