import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/auth/auth-services/auth.service';
import { Room } from '../room';
import { RoomService } from '../room-services/room.service';

@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html',
  styleUrls: ['./room-detail.component.css']
})
export class RoomDetailComponent implements OnInit {

  @Input() room: Room;
  dungeonId: string;
  roomId: string;
  token: string;
  userId: string;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private roomService: RoomService,
    private authservice: AuthService
  ) { }

  ngOnInit(): void {
    this.getRoom()
    this.dungeonId = this.route.snapshot.paramMap.get('dungeonId');
    this.roomId = this.route.snapshot.paramMap.get('roomId');
    this.token = this.authservice.currentUser$.value.token;
    this.userId = this.authservice.currentUser$.value._id;
  }

  getRoom(): void{
      this.roomService.getRoom(this.route.snapshot.paramMap.get('dungeonId'), this.route.snapshot.paramMap.get('roomId'))
        .subscribe(room => this.room = room);
  }

  delete(room: Room): void{
    this.roomService.deleteRoom(this.dungeonId, room, this.token).
      subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }

}
