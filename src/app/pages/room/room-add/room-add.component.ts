import { Component, OnInit } from '@angular/core';
import { RoomService } from '../room-services/room.service';

import { Environments } from '../../../enums/environments.enum'
import { Room } from '../room'
import { Location } from '@angular/common';
import { FormBuilder } from '@angular/forms'
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/auth/auth-services/auth.service';

@Component({
  selector: 'app-room-add',
  templateUrl: './room-add.component.html',
  styleUrls: ['./room-add.component.css']
})
export class RoomAddComponent implements OnInit {

  dungeonId: string; 
  
  model = new Room("new room", "description", 10, false);
  token: string;


  constructor(
    private route: ActivatedRoute,
    private roomService: RoomService,
    private location: Location,
    private authService: AuthService
  ) {

   }

  ngOnInit(): void {
    this.token = this.authService.currentUser$.value.token
  }

  onSubmit(){
    this.dungeonId = this.route.snapshot.paramMap.get('dungeonId');
    console.log(this.model);
    this.roomService.addRoom(this.model, this.dungeonId, this.token)
      .subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }

}
