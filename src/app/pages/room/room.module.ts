import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from './index.room';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MinDirective } from './room-directives/min.directive';
import { MaxDirective} from './room-directives/max.directive';

const routes: Routes = [
  { path: ':roomId/dungeon/:dungeonId', pathMatch: 'full', component: fromComponents.RoomDetailComponent},
  { path: 'dungeon/:dungeonId', pathMatch: 'full', component: fromComponents.RoomAddComponent},
  { path: ':roomId/dungeon/:dungeonId/edit', pathMatch: 'full', component: fromComponents.RoomEditComponent},
];

@NgModule({
  declarations: [...fromComponents.components, MinDirective, MaxDirective],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule, 
    RouterModule.forChild(routes),
  ]
})
export class RoomModule { }
