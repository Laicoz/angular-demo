import { Injectable } from '@angular/core';

import {Observable, of} from 'rxjs';

import {Room} from '../room';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  private dungeonShareUrl = 'https://dungeonshareapi.herokuapp.com'

  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      Authorization: 'placeholder' 
    })
  }

  private handleError<T>(operation = 'operation', result?:T){
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }

  constructor(
    private http: HttpClient
  ) { }

  addRoom(room: Room, id: string, token: string): Observable<Room>{
    this.setHttpOptions(token);
    const url = `${this.dungeonShareUrl}/dungeon/${id}/room`;
    console.log(url, room);
    return this.http.post<Room>(url, room, this.httpOptions).pipe(
      catchError(this.handleError<Room>('addRoom'))
    );
  }

  
  getRoom(dungeonId: string, roomId: string): Observable<Room>{
    const url = `${this.dungeonShareUrl}/dungeon/${dungeonId}/room/${roomId}`;
    return this.http.get<Room>(url).pipe(
      catchError(this.handleError<Room>(`getRoom`))
    );
  }

  deleteRoom(dungeonId: string, room: Room | string, token: string): Observable<Room>{
    this.setHttpOptions(token);
    const roomId = typeof room === 'string' ? room : room._id;
    const url = `${this.dungeonShareUrl}/dungeon/${dungeonId}/room/${roomId}`;
    console.log(url)

    return this.http.delete<Room>(url, this.httpOptions).pipe(
      catchError(this.handleError<Room>('deleteRoom'))
    );
  }

  updateRoom(room: Room, dungeonId: string, token: string): Observable<any> {
    this.setHttpOptions(token);
    const url = `${this.dungeonShareUrl}/dungeon/${dungeonId}/room/${room._id}`
    console.log(room)
    console.log(url)
    return this.http.put(url, room, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateDungeon'))
    );
  }

  setHttpOptions(token: String){
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Bearer ${token}`);
  }

}
