import { Component, OnInit } from '@angular/core';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly ADMIN_USER = 'Administrator';

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd',
    },
    {
      id: 'UC-02',
      name: 'Registreren',
      description: 'Een gebruiker maakt een nieuw account aan.',
      scenario: [
        'Gebruiker vult email en wachtwoord in. Gebruiker herhaalt het wachtwoord en klikt op registreren.',
        'De applicatie controleert of het herhaalde wachtwoord overeenkomt, en of de gebruikersnaam al eerder gekozen is.',
        'Als de gegevens correct zijn, is er een nieuw account aangemaakt en naar het startscherm genavigeerd.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Gebruiker heeft nog geen account.',
      postcondition: 'Actor heeft een nieuw account en is ingelogd.'
    },
    {
      id: 'UC-03',
      name: 'Dungeon aanmaken.',
      description: 'Een gebruiker maakt een nieuw account aan.',
      scenario: [
        'Gebruiker vult gegevens in voor een nieuwe dungeon en klikt op de save knop.',
        'Applicatie controleert of alle gegevens zijn ingevoerd. Controlleert of Size, Difficulty en \'Preferred Number of Players\' 1 of hoger is. Controlleert of de naam langer is dan 3 tekens.',
        'Als de gegevens correct zijn, wordt er een nieuwe dungeon aangemaakt en wordt de detailpagina van de dungeon getoond.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Gebruiker is ingelogd.',
      postcondition: 'Er is succesvol een nieuwe dungeon gemaakt met de gebruiker als eigenaar.'
    },
    {
      id: 'UC-04',
      name: 'Dungeon aanpassen.',
      description: 'Een gebruiker past een dungeon aan.',
      scenario: [
        'Gebruiker wijzigt de gegevens van de dungeon en klikt op de save knop.',
        'Applicatie controleert of de gebruiker de eigenaar van de dungeon is. Applicatie controleert of alle gegevens ingevoerd zijn. Controleert of Size, Difficulty en \'Preferred Number of Players\' 1 of hoger is. Controleert of de naam langer is dan 3 tekens.',
        'Als de gegevens correct zijn, wordt de dungeon aangepast en wordt de detailpagina van de dungeon getoond.'
    ],
      actor: this.PLAIN_USER,
      precondition: 'Gebruiker is ingelogd, en eigenaar van de dungeon.',
      postcondition: 'De gebruiker heeft de dungeon aangepast.'
    },
    {
      id: 'UC-05',
      name: 'Dungeon verwijderen.',
      description: 'Een gebruiker verwijdert een dungeon.',
      scenario: [
        'Gebruiker klikt op de knop verwijderen.',
        'Systeem controleert of de gebruiker eigenaar is van de dungeon.',
        'Systeem laat een prompt zien of de gebruiker zeker is. Hierin is ook vermeld dat alle rooms in de dungeon worden verwijderd.',
        'Gebruiker klikt op de knop \'Ik weet het zeker\'.',
        'De applicatie verwijderd de dungeon en alle bijbehorende rooms. Gebruiker komt terug op het overzicht van dungeons.'
    ],
      actor: this.PLAIN_USER,
      precondition: 'Gebruiker is ingelogd en eigenaar van de dungeon.',
      postcondition: 'Dungeon en alle bijbehorende rooms zijn succesvol verwijderd.'
    },
    {
      id: 'UC-06',
      name: 'Dungeon overzicht.',
      description: 'Een gebruiker krijgt een overzicht van alle dungeons.',
      scenario: [
        'Gebruiker navigeert naar het overzicht van dungeons en vult eventueel filtergegevens in.',
        'Applicatie toont dungeons op basis van de filergegevens.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'Gebruiker ziet een overzicht van verschillende dungeons.'
    },
    {
      id: 'UC-07',
      name: 'Room toevoegen.',
      description: 'Een gebruiker voegt een room toe aan een dungeon.',
      scenario: [
        'Gebruiker selecteert de dungeon waar een room aan wordt teogevoegd en klikt op toevoegen.',
        'Applicatie controlleert of de gebruiker eigenaar is van de dungeon en toont het formulier.',
        'Gebruiker voert de gegevens in en klikt op save.',
        'Applicatie controleert de gegevens. Name met ingevuld zijn. Als hidden niet is ingevuld wordt deze op false gezet.',
        'Gebruiker wordt naar de detailpagina van de room gebracht.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Gebruiker is ingelogd en heeft al een dungeon aangemaakt.',
      postcondition: 'Room is toegevoegd aan de dungeon.'
    },
    {
      id: 'UC-08',
      name: 'Room aanpassen.',
      description: 'Een gebruiker past een bestaande room aan.',
      scenario: [
        'Gebruiker selecteert de aan te passen room en klikt op wijzigen.',
        'Applicatie controlleert of de gebruiker eigenaar is van de room.',
        'Gebruiker voert de gegevens in en klikt op save.',
        'Applicatie controleert de gegevens. Name moet ingevuld zijn. Als hidden niet is ingevuld wordt deze op false gezet.',
        'Gebruiker wordt teruggebracht naar de detailpagina van de room.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Gebruiker is ingelogd en heeft al een room aangemaakt.',
      postcondition: 'Room is succesvol toegevoegd.'
    },
    {
      id: 'UC-09',
      name: 'Room verwijderen.',
      description: 'Een gebruiker verwijdert een bestaande room.',
      scenario: [
        'Gebruiker selecteert de room en klikt op verwijderen.',
        'Applicatie controleert of de gebruiker de eigenaar is van de room.',
        'Applicatie verwijdert de room en de gebruiker wordt teruggebbracht naar de detailpagina van de dungeon.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Gebruiker is ingelogd. Gebruiker is eigenaar van de room.',
      postcondition: 'Room is succesvol verwijderd.'
    },
    {
      id: 'UC-10',
      name: 'Monster toevoegen.',
      description: 'Een gebruiker voegt een monster toe aan een room.',
      scenario: [
        'Gebruiker selecteert de room waar een monster aan wordt toegevoegd en klikt op toevoegen.',
        'Applicatie controleert of de gebruiker eigenaar is van de room en toont het formulier.',
        'Gebruiker voert de gegevens in en klikt op save.',
        'Applicatie controlleert de gegevens. Alle gegevens moeten ingevuld zijn. Challenge moet 0 of hoger zijn. HP en AC moeten 1 of hoger zijn.',
        'Gebruiker wordt teruggebracht naar de detailpagina van het monster.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Gebruiker is ingelogd en heeft al een room aangemaakt.',
      postcondition: 'Monster is toegevoegd aan de room.'
    },
    {
      id: 'UC-11',
      name: 'Monster verwijderen van room.',
      description: 'Een gebruiker verwijdert een monster van een room.',
      scenario: [
        'Gebruiker selecteert de room en klikt bij het monster op verwijderen.',
        'Applicatie controleert of de gebruiker eigenaar is van de room.',
        'Applicatie verwijdert het monster van de room en de gebruiker wordt teruggebracht naar de detailpagina van de room.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Gebruiker is ingelogd en heeft een monster toegevoegd aan een room.',
      postcondition: 'Monster is verwijderd van de room.'
    }
  ];

  constructor() {}

  ngOnInit(): void {}
}
